import os, sys, math
from PIL import Image

im = Image.open(sys.argv[1])

sW, sH = im.size
im_arr = []

avg = lambda z: sum(z)//len(z)

for i in range(sW):
    #im_arr.append([])
    for j in range(sH):
        #im_arr[-1].append(im.getpixel((i,j)))
        im_arr.append(avg(im.getpixel((j,i))))

outrgb = [im_arr[j//3] for j in range(3*len(im_arr))]
#print(outrgb)

q = Image.frombytes("RGB", (sW,sH), bytes(outrgb))
q.save(sys.argv[2])
print("finished running,", sW, sH)

all: a.out
a.out: *.v
	iverilog conv.v conv_top.v conv_tb_top_iv.v axi_emu.v HullFIFO.v
.PHONY: test
test: a.out
	./a.out
	diff mem_out.hex test.hex

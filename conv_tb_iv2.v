module tb;

// Clock and reset
reg clk = 0;
always #5 clk = !clk;
reg [31:0] count = 0;
wire rst = count < 2;
always @(posedge clk) count <= count + 1;

// Configuration

// Image size
localparam TRANS_SIZE = 128;
localparam I_SIZE = 32;

reg valid_in;
reg [TRANS_SIZE-1:0] image;
reg signed [71:0] kernel;
reg [7:0] krnl_shift;

// DUT Outputs
wire valid_out;
wire [TRANS_SIZE-1:0] image_out;

// Logic
always @(*) begin
    if (count > 5) begin
		valid_in = 1;
		image =  128'hff_fe_fd_fc_fb_fa_f0_e0_07_06_05_04_03_02_01_dd;
		kernel = 72'h00_00_00_00_01_00_00_00_00;
        krnl_shift = 0;
	end else begin
		valid_in = 0;
		image =  'h000000000000000000;
		kernel = 'h000000000000000000;
	end
end
always @(posedge clk) begin
	if (valid_out) begin
		$display("valid output: %h", image_out);
        $display("on cycle %d", count);
	end
	if (count == 32'd100) $finish(0);
end

wire ready;

// DUT
Grid #(
    .TRANS_SIZE(TRANS_SIZE),
	.I_SIZE(I_SIZE)
) dut (
	.clk(clk),
	.rst(rst),
	
	.valid_in(valid_in),
	.image(image),
	.kernel(kernel),
    .krnl_shift(krnl_shift),
    .krnl_loaded(valid_in),
	
	.valid_out(valid_out),
	.image_out(image_out),
    .ready(ready)
);

endmodule

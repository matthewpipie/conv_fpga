module tb;

// Clock and reset
reg clk = 0;
always #5 clk = !clk;
reg [31:0] count = 0;
wire rst = count < 2;
always @(posedge clk) count <= count + 1;


reg valid_in;
reg [71:0] localPixels_mem;
reg signed [71:0] kernel;
reg [7:0] krnl_shift;

// DUT Outputs
wire valid_out;
wire [7:0] o_color;

// Logic
always @(*) begin
    if (count > 5) begin
		valid_in = 1;
		localPixels_mem =  72'h01_02_03_04_05_06_07_08_09;
		kernel =           72'h00_00_FF_00_02_00_00_00_00;
        krnl_shift = 0;
	end else begin
		valid_in = 0;
		localPixels_mem =  'h000000000000000000;
		kernel = 'h000000000000000000;
	end
end
always @(posedge clk) begin
	if (valid_out) begin
		$display("valid output: %h", o_color);
        $display("on cycle %d", count);
	end
	if (count == 32'd10) $finish(0);
end

wire ready;

// DUT
Pixel dut (
	.clk(clk),
	.rst(rst),
	.valid(valid_in),

	.localPixels_mem(localPixels_mem),

	.kernel(kernel),
    .krnl_shift(krnl_shift),
	
	.o_color(o_color),
	.valid_out(valid_out)
);

/*
    // Clock
    input wire clk,
    input wire rst,
    input wire valid,

    // pixel inputs
    input wire [71:0] localPixels_mem,

    // kernel
    input wire signed [71:0] kernel,
    input wire [7:0] krnl_shift,

    // output
    output reg [7:0] o_color,
    output reg valid_out
*/

endmodule

import os, sys, math
from PIL import Image

def conv_to_arr(st, signed):
    spl = [st[i:i+2] for i in range(0, len(st), 2)]
    return list(map(lambda y: y-256 if signed and y >= 128 else y, map(lambda x: int(x,16), reversed(spl))))

im_arr = []
cnt = 0
with open(sys.argv[1]) as hexfile:
    line = hexfile.readline().strip()
    while line:
        if "//" not in line:
            im_arr.extend(conv_to_arr(line, False));
            cnt += 1
        line = hexfile.readline().strip()

i = int(math.sqrt(cnt*64))

#print(im_arr, size)

outrgb = [im_arr[j//3] for j in range(3*len(im_arr))]
#print(outrgb)

q = Image.frombytes("RGB", (i,i), bytes(outrgb))
q.save(sys.argv[2])
print("finished running,", i)

`include "conv_constants.v"
// hardcoded parameters - 1-byte color width, 3x3 kernel

module ConvPixel
#(parameter DEBUG = 0) (
    // Clock
    input wire clk,
    input wire rst,
    //input wire valid, // moved to convgrid

    // pixel inputs
    input wire [71:0] localPixels_mem,

    // kernel
    input wire [71:0] kernel_in,
    input wire [7:0] krnl_shift_in,

    input wire ready_out,
    // output
    output reg [7:0] o_color
    //output reg valid_out // moved to convgrid
);

    // stage 0 - store
    reg [71:0] pixels0;
    reg [71:0] kernel0;
    reg [7:0] krnl_shift0;
    reg [71:0] pixels;
    reg [71:0] kernel;
    reg [7:0] krnl_shift;
    //reg valid_0;
    always @(posedge clk) begin
        //if (rst) begin
            //valid_0 <= 0;
        //end else
        //if (ready_out) begin
            //valid_0 <= valid;
            pixels0 <= localPixels_mem;
            pixels <= pixels0;
            kernel0 <= kernel_in;
            kernel <= kernel0;
            krnl_shift0 <= krnl_shift_in;
            krnl_shift <= krnl_shift0;
            if (DEBUG) begin
                $display("%h", localPixels_mem);
            end
        //end
    end


    // stage 1 - multiply
    wire signed [19:0] mult_ [8:0];
    reg signed [19:0] mult [8:0];
    //reg valid_1;

    genvar pix_1;
    for (pix_1 = 0; pix_1 < 9; pix_1 = pix_1 + 1) begin: PIXEL_1
        wire signed [8:0] pixel = {1'b0, pixels[pix_1*8 +: 8]};
        wire signed [7:0] krnl_pix = kernel[pix_1*8 +: 8];
        wire signed [15:0] mult_short = pixel * krnl_pix;
        assign mult_[pix_1] = { {(4){mult_short[15]}}, mult_short[15:0]};
    end
    always @(posedge clk) begin
        /*if (rst) begin
            valid_1 <= 0;
        end else 
        if (ready_out) begin
            valid_1 <= valid_0;
        end*/

    end

    genvar pix_1_;
    for (pix_1_ = 0; pix_1_ < 9; pix_1_ = pix_1_ + 1) begin
        always @(posedge clk) /*if (ready_out)*/ mult[pix_1_] <= mult_[pix_1_];
    end


    // stage 2 - accumulate

    reg signed [19:0] basicSums [10:0];
    wire signed [19:0] sum;
    //reg signed [19:0] basicSums [5:3];
    //reg valid_2;

    always @(posedge clk) begin

        //if (ready_out) begin
            basicSums[0] <= mult[0] + mult[1];
            basicSums[1] <= mult[2] + mult[3];
            basicSums[2] <= mult[4] + mult[5];
            basicSums[3] <= mult[6] + mult[7];
            basicSums[4] <= mult[8];

            basicSums[5] <= basicSums[0] + basicSums[1];
            basicSums[6] <= basicSums[2] + basicSums[3];
            basicSums[7] <= basicSums[4];

            basicSums[8] <= basicSums[5] + basicSums[6];
            basicSums[9] <= basicSums[7];

            basicSums[10] <= basicSums[8] + basicSums[9];
        //end
    end
    assign sum = basicSums[10];

    //genvar pix_2_;
    //for (pix_2_ = 3; pix_2_ <= 5; pix_2_ = pix_2_ + 1) begin
        //always @(posedge clk) if (ready_out) basicSums[pix_2_] <= basicSums_[pix_2_];
    //end


    // stage 3 - shift

    reg signed [19:0] result_div;
    reg [7:0] result;
    // Register output score
    always @(posedge clk) begin
        //if (rst) begin
            //o_color <= 0;
            //valid_out <= 0;
        //end else 
        //if (ready_out) begin
            result_div <= sum >>> krnl_shift;
            result <= ((result_div[19]) ? 0 :
                                |result_div[19:8] ? {(8){1'b1}} : 
                                result_div[7:0]);

            o_color <= result;
        //end
    end
endmodule

module ConvGrid #(
    parameter TRANS_SIZE = 512,
	// Image size, MUST be a multiple of (TRANS_SIZE_B)
	parameter I_SIZE = 64
) (
	// Clock and reset
	input wire clk,
	input wire rst,
	
	// Inputs
	input wire valid_in,
	input wire [TRANS_SIZE-1:0] image, // we get in 64 bytes per cycle
	input wire [71:0] kernel,
    input wire [7:0] krnl_shift,
    input wire krnl_loaded,
	
    input wire ready_out,
	// Outputs
	output wire valid_out,
	output wire [TRANS_SIZE-1:0] image_out,
    input wire done
);
    localparam TRANS_SIZE_B = TRANS_SIZE/8;
    localparam BUFSIZ = 2*I_SIZE + 3*TRANS_SIZE_B;
    localparam IND = $clog2(BUFSIZ);
    reg [7:0] mem [BUFSIZ-1:0];
    reg [$clog2(I_SIZE+10)-1:0] readRow;
    reg [$clog2(I_SIZE+10)-1:0] rowsDone;
    reg [$clog2(I_SIZE*I_SIZE/*/8*/ + 100)-1:0] cycles; // +10 is just for safety

    wire live_next = cycles+1 > ((I_SIZE/TRANS_SIZE_B) + 1);
    wire momentum_next = cycles+1 >= I_SIZE*(I_SIZE/TRANS_SIZE_B); //TODO
    wire notPast_next = cycles+1 <= (I_SIZE + 1)*(I_SIZE/TRANS_SIZE_B) + 1; // TODO

    reg live;
    reg momentum;
    reg notPast;

    integer ii, jk;
    always @(posedge clk) begin
        if (rst) begin
            readRow <= 0;
            rowsDone <= 0;
            cycles <= 0;
            live <= 0;
            momentum <= 0;
            notPast <= 1;
        end else if (done) begin
            readRow <= 0;
            rowsDone <= 0;
            cycles <= 0;
            live <= 0;
            momentum <= 0;
            notPast <= 1;
        end else begin
            if (ready_out && valid_in) begin
                for (ii = 0; ii < TRANS_SIZE_B; ii = ii + 1) begin
                    mem[(BUFSIZ - TRANS_SIZE_B) + ii] <= image[ii*8+:8];
                end
                //writeI <= writeI == BUFSIZ-TRANS_SIZE_B ? 0 : writeI + TRANS_SIZE_B;
            end
            if (ready_out && (valid_in || momentum)) begin // when we've loaded all memory, we don't need to rely on valid_in
                cycles <= cycles + 1;
                live <= live_next;
                momentum <= momentum_next;
                notPast <= notPast_next;
                for (jk = 0; jk < BUFSIZ - TRANS_SIZE_B; jk = jk + 1) begin
                    mem[jk] <= mem[jk + TRANS_SIZE_B];
                end
                if (live) begin
                    readRow <= readRow == I_SIZE-TRANS_SIZE_B ? 0 : readRow + TRANS_SIZE_B;
                    rowsDone <= rowsDone + (readRow == I_SIZE-TRANS_SIZE_B ? 1 : 0);
                end
            end
        end
    end
    wire goodInput = ready_out && live && notPast && (valid_in || momentum);
    wire topIsEdge = rowsDone == 0;
    wire bottomIsEdge = rowsDone == I_SIZE - 1;
    wire leftIsEdge = readRow == 0;
    wire rightIsEdge = readRow == I_SIZE-TRANS_SIZE_B;
    genvar pix, i;
    for (pix = 0; pix < TRANS_SIZE_B; pix = pix + 1) begin: PIXELS

        wire myLeftIsEdge = leftIsEdge && pix == 0;
        wire myRightIsEdge = rightIsEdge && pix == TRANS_SIZE_B-1;
        wire [7:0]localPixels[8:0];
        wire [71:0]localPixels_mem;

        localparam myReadI = I_SIZE + TRANS_SIZE_B + pix;
        localparam topMemAddr = myReadI - I_SIZE;
        localparam bottomMemAddr = myReadI + I_SIZE;

        wire [8:0]valids;

        assign valids[0] = !(myLeftIsEdge || topIsEdge);
        assign valids[1] = !topIsEdge;
        assign valids[2] = !(myRightIsEdge || topIsEdge);
        assign valids[3] = !myLeftIsEdge;
        assign valids[4] = 1;
        assign valids[5] = !myRightIsEdge;
        assign valids[6] = !(myLeftIsEdge || bottomIsEdge);
        assign valids[7] = !bottomIsEdge;
        assign valids[8] = !(myRightIsEdge || bottomIsEdge);

        assign localPixels[0] = valids[0] ? mem[topMemAddr    - 1] : 0;
        assign localPixels[1] = valids[1] ? mem[topMemAddr       ] : 0;
        assign localPixels[2] = valids[2] ? mem[topMemAddr    + 1] : 0;
        assign localPixels[3] = valids[3] ? mem[myReadI       - 1] : 0;
        assign localPixels[4] = valids[4] ? mem[myReadI          ] : 0;
        assign localPixels[5] = valids[5] ? mem[myReadI       + 1] : 0;
        assign localPixels[6] = valids[6] ? mem[bottomMemAddr - 1] : 0;
        assign localPixels[7] = valids[7] ? mem[bottomMemAddr    ] : 0;
        assign localPixels[8] = valids[8] ? mem[bottomMemAddr + 1] : 0;

        for (i = 0; i < 9; i = i + 1) begin
            assign localPixels_mem[8*i +: 8] = localPixels[i];
        end

        //ConvPixel #(.DEBUG(pix == (0))) pixel (
        ConvPixel #(.DEBUG(0)) pixel (
            .clk(clk),
            .rst(rst),
            //.valid(goodInput),
            .localPixels_mem(localPixels_mem),
            .kernel_in(kernel),
            .krnl_shift_in(krnl_shift),
            .ready_out(ready_out),
            .o_color(image_out[pix*8 +: 8])
            //.valid_out(valid_out_all[pix])
        );
    end

    // pixel stage bookkeeping
    reg [`PIPELINE_SIZE - 1:0] validP;
    integer ppp;
    always @(posedge clk) begin
        if (rst) begin
            validP <= 0;
        end else /*if (ready_out)*/ begin
            validP[0] <= goodInput;
	    for (ppp = 1; ppp < `PIPELINE_SIZE; ppp = ppp + 1) begin
		validP[ppp] <= validP[ppp - 1];
	    end
        end
    end
/*
    genvar ppp;
    for (ppp = 1; ppp < `PIPELINE_SIZE; ppp = ppp + 1) begin
        always @(posedge clk) / *if (ready_out)* / validP[ppp] <= validP[ppp - 1];
    end
*/

    assign valid_out = validP[`PIPELINE_SIZE - 1];// && ready_out;

    always @(posedge clk) begin
        //$display("mem[0] and mem[1] are %h %h", mem[0], mem[1]);
        /*
        $write("mem: ");
        for (jk = 0; jk < BUFSIZ; jk = jk + 1) begin
            $write("%h",mem[jk]);
        end
        $display("");
        $display("cycles: %d", cycles);
        $display("writeI: %d", writeI);
        $display("readI: %d", readI);
        $display("readRow: %d", readRow);
        $display("rowsDone: %d", rowsDone);
        $display(" good?: %h", PIXELS[0].goodInput);
        $display(" 0: %h", PIXELS[0].localPixels_mem);
        $display(" 7: %h", PIXELS[7].localPixels_mem);
        */
        //$display(" in is %d", PIXELS[0].goodInput);
        //$display(" out0 is %d", valid_out_all[0]);
    end


endmodule

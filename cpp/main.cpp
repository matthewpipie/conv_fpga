#include <iostream>
#include <fstream>

const int IMAGE_SIZE = 128;
const int IMAGE_SIZE2 = IMAGE_SIZE*IMAGE_SIZE;

int main(int argc, char *argv[]) {
    signed char kernel[10];
    const int IL = 1;
    const int OL = 2;
    const int KL = 3;
    for (int i = 0; i < 10; i++) {
        kernel[i] = (char)std::stoi(argv[KL + i], nullptr, 16);
    }
    
    char *inputFN = argv[IL];
    char *outFN = argv[OL];
    std::ifstream in_io;
    in_io.open(inputFN, std::ios::in | std::ios::binary);
    char *image_data_raw = new char[IMAGE_SIZE2];
    in_io.read(image_data_raw, IMAGE_SIZE2 * sizeof(char));
    if (!in_io) std::cout << "err! " << in_io.gcount()  << " " << inputFN<< std::endl;
    in_io.close();

    unsigned char *image_data = (unsigned char*)image_data_raw;

    std::ofstream out_io(outFN, std::ios::out | std::ios::binary | std::ios::trunc);
    // now we have image and kernel loaded, begin
    for (uint32_t pixel = 0; pixel < IMAGE_SIZE2; pixel++) {
        char out[64];
        short mult[9];
        unsigned short surrounding[9];
        uint32_t topAddr = pixel - IMAGE_SIZE;
        uint32_t btmAddr = pixel + IMAGE_SIZE;
        bool onTop = pixel < IMAGE_SIZE;
        bool onLeft = pixel % IMAGE_SIZE == 0;
        bool onBtm = pixel > IMAGE_SIZE2 - IMAGE_SIZE;
        bool onRight = pixel % IMAGE_SIZE == IMAGE_SIZE - 1;
        surrounding[0] = onLeft  || onTop ? 0 : image_data[topAddr - 1];
        surrounding[1] = onTop            ? 0 : image_data[topAddr    ];
        surrounding[2] = onRight || onTop ? 0 : image_data[topAddr + 1];
        surrounding[3] = onLeft           ? 0 : image_data[pixel   - 1];
        surrounding[4] =                        image_data[pixel      ];
        surrounding[5] = onRight          ? 0 : image_data[pixel   + 1];
        surrounding[6] = onLeft  || onBtm ? 0 : image_data[btmAddr - 1];
        surrounding[7] = onBtm            ? 0 : image_data[btmAddr    ];
        surrounding[8] = onRight || onBtm ? 0 : image_data[btmAddr + 1];
        if (pixel == 0) {
            std::cout << onTop << " " << onLeft << " " << onBtm << " " << onRight << " "  << std::endl;
            for (int i = 0; i < 9; i++)
                std::cout << surrounding[i] << " ";
        }
        for (char i = 0; i < 9; i++) {
            mult[i] = (signed short)surrounding[i] * (signed short)kernel[i];
        }
        signed int sum = 0;
        for (char i = 0; i < 9; i++) {
            sum += mult[i];
        }
        sum >>= (unsigned char)kernel[9];
        char result = sum < 0 ? 0 : (sum > 255 ? 255 : ((char)sum));
        out[63 - (pixel % 64)] = result;
        if (pixel % 64 == 63) {
            out_io.write(out, 64);
            //out_io << result;
        }
    }

    return 0;
}

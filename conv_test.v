

module Grid #(
	// Image size, MUST be a multiple of 8
	parameter I_SIZE = 8,
    parameter I_SIZE2 = I_SIZE*I_SIZE
) (
	// Clock and reset
	input wire clk,
	input wire rst,
	
	// Inputs
	input wire valid_in,
	input wire [63:0] image, // we get in 64 bytes per cycle
	input wire signed [71:0] kernel,
    input wire [7:0] k_div,
    input wire krnl_loaded,
	
	// Outputs
	output wire valid_out,
	output wire [63:0] image_out,
    output wire ready
)

import os, sys, math
from pathlib import Path
from PIL import Image

im = Image.open(sys.argv[1])

sW, sH = im.size
im_arr = []

avg = lambda z: sum(z)//len(z)
for i in range(sW):
    #im_arr.append([])
    for j in range(sH):
        #im_arr[-1].append(im.getpixel((i,j)))
        im_arr.append(avg(im.getpixel((j,i))))

im_split = [''.join(map(lambda x: '%02x' % x, reversed(im_arr[i:i+64]))) for i in range(0, len(im_arr), 64)]
#img_str_all = ''.join(map(lambda x: '%02x' % x, im_arr))
#img_str_split = [img_str_all[i:i+128] for i in range(0, len(img_str_all), 128)]
print(len(im_arr), im.size)
#img_str = "\n".join(img_str_split).strip()
img_str = "\n".join(im_split).strip()
Path(sys.argv[2]).write_text(img_str)

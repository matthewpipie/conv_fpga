`include "conv_constants.v"
`define MEM_WORDS (`IMAGE_SIZE*`IMAGE_SIZE/64 * 2)
module tb_top;

// Clock and reset
reg clk = 0;
always #5 clk = !clk;
reg [31:0] count = 0;
wire rst = count < 2;
always @(posedge clk) count <= count + 1;


// AXI memory interface
wire [15:0] arid_m;
wire [63:0] araddr_m;
wire [7:0]  arlen_m;
wire [2:0]  arsize_m;
wire        arvalid_m;
wire        arready_m;

wire [15:0]  rid_m;
wire [511:0] rdata_m;
wire [1:0]   rresp_m;
wire         rlast_m;
wire         rvalid_m;
wire         rready_m;

wire [15:0] awid_m;
wire [63:0] awaddr_m;
wire [7:0]  awlen_m;
wire [2:0]  awsize_m;
wire        awvalid_m;
wire        awready_m;

wire [15:0]  wid_m;
wire [511:0] wdata_m;
wire [63:0]  wstrb_m;
wire         wlast_m;
wire         wvalid_m;
wire         wready_m;

wire [15:0] bid_m;
wire [1:0]  bresp_m;
wire        bvalid_m;
wire        bready_m;

// SoftReg interface
reg        softreg_req_valid;
reg        softreg_req_isWrite;
reg [31:0] softreg_req_addr;
reg [63:0] softreg_req_data;

wire        softreg_resp_valid;
wire [63:0] softreg_resp_data;

always @(*) begin
	case (count)
	32'd3: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 1;
		softreg_req_addr = 32'h00;
		softreg_req_data = 64'h0; // image address
	end
	32'd8: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 1;
		softreg_req_addr = 32'h08;
		softreg_req_data = `IMAGE_SIZE*`IMAGE_SIZE; // image write address
	end
	32'd37: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 1;
		softreg_req_addr = 32'h10;
		softreg_req_data = 64'h02_01_02_04_02_01_02_01; // lower 8 bytes of kernel
		//softreg_req_data = 64'hFF_00_FF_05_FF_00_FF_00; // lower 8 bytes of kernel - SHARPEN
	end
	32'd49: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 1;
		softreg_req_addr = 32'h18;
		softreg_req_data = 64'h04_01; // krnl_shift and upper byte of kernel
		//softreg_req_data = 64'h00_00; // krnl_shift and upper byte of kernel - SHARPEN
	end
	32'd2000000: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 0;
		softreg_req_addr = 32'h108;
		softreg_req_data = 64'h0;
	end
/*
	32'd450: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 1;
		softreg_req_addr = 32'h00;
		softreg_req_data = 64'h0; // image address
	end
	32'd500: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 1;
		softreg_req_addr = 32'h08;
		softreg_req_data = 2 * `IMAGE_SIZE*`IMAGE_SIZE; // image write address
	end
	32'd550: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 1;
		softreg_req_addr = 32'h10;
		softreg_req_data = 64'h02_01_02_04_02_01_02_01; // lower 8 bytes of kernel
		//softreg_req_data = 64'hFF_00_FF_05_FF_00_FF_00; // lower 8 bytes of kernel - SHARPEN
	end
	32'd600: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 1;
		softreg_req_addr = 32'h18;
		softreg_req_data = 64'h04_01; // krnl_shift and upper byte of kernel
		//softreg_req_data = 64'h00_00; // krnl_shift and upper byte of kernel - SHARPEN
	end
	32'd1000: begin
		softreg_req_valid = 1;
		softreg_req_isWrite = 0;
		softreg_req_addr = 32'h108;
		softreg_req_data = 64'h0;
	end
*/
	default: begin
		softreg_req_valid = 0;
		softreg_req_isWrite = 0;
		softreg_req_addr = 0;
		softreg_req_data = 0;
	end
	endcase
end
reg deb = 1;
always @(posedge clk) begin
	if (softreg_resp_valid && deb == 0) begin
		deb <= 1;
	end else if (softreg_resp_valid || softreg_resp_data == 12345679) begin
		$display("writes left: %d", softreg_resp_data);
        //$writememh("./mem_out.hex", ae.mem, `MEM_WORDS/2);
		$finish(0);
	end
end


// instantiations
axi_emu #(
	//.WORDS(`MEM_WORDS)
	.WORDS(128)
) ae (
	.clk(clk),
	.rst(rst),
	
	.arid_m(arid_m),
	.araddr_m(araddr_m),
	.arlen_m(arlen_m),
	.arsize_m(arsize_m),
	.arvalid_m(arvalid_m),
	.arready_m(arready_m),
	
	.rid_m(rid_m),
	.rdata_m(rdata_m),
	.rresp_m(rresp_m),
	.rlast_m(rlast_m),
	.rvalid_m(rvalid_m),
	.rready_m(rready_m),
	
	.awid_m(awid_m),
	.awaddr_m(awaddr_m),
	.awlen_m(awlen_m),
	.awsize_m(awsize_m),
	.awvalid_m(awvalid_m),
	.awready_m(awready_m),
	
	.wid_m(wid_m),
	.wdata_m(wdata_m),
	.wstrb_m(wstrb_m),
	.wlast_m(wlast_m),
	.wvalid_m(wvalid_m),
	.wready_m(wready_m),
	
	.bid_m(bid_m),
	.bresp_m(bresp_m),
	.bvalid_m(bvalid_m),
	.bready_m(bready_m)
);

conv_top ct (
	.clk(clk),
	.rst(rst),
	
	.arid_m(arid_m),
	.araddr_m(araddr_m),
	.arlen_m(arlen_m),
	.arsize_m(arsize_m),
	.arvalid_m(arvalid_m),
	.arready_m(arready_m),
	
	.rid_m(rid_m),
	.rdata_m(rdata_m),
	.rresp_m(rresp_m),
	.rlast_m(rlast_m),
	.rvalid_m(rvalid_m),
	.rready_m(rready_m),
	
	.awid_m(awid_m),
	.awaddr_m(awaddr_m),
	.awlen_m(awlen_m),
	.awsize_m(awsize_m),
	.awvalid_m(awvalid_m),
	.awready_m(awready_m),
	
	.wid_m(wid_m),
	.wdata_m(wdata_m),
	.wstrb_m(wstrb_m),
	.wlast_m(wlast_m),
	.wvalid_m(wvalid_m),
	.wready_m(wready_m),
	
	.bid_m(bid_m),
	.bresp_m(bresp_m),
	.bvalid_m(bvalid_m),
	.bready_m(bready_m),
	
	.softreg_req_valid(softreg_req_valid),
	.softreg_req_isWrite(softreg_req_isWrite),
	.softreg_req_addr(softreg_req_addr),
	.softreg_req_data(softreg_req_data),
	
	.softreg_resp_valid(softreg_resp_valid),
	.softreg_resp_data(softreg_resp_data)
);

endmodule

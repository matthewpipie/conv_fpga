#include <chrono>
#include <cstring>
#include "aos.hpp"

using namespace std::chrono;

const uint64_t I_SIZE[4] = {128, 8192, 57344, 196608};
#define sqr(i) (I_SIZE[i]*I_SIZE[i])
const uint64_t I_SIZE2[4] = {sqr(0), sqr(1), sqr(2), sqr(3)};

const uint64_t SLEEP_TIME[4] = {1, 50, 100, 100};

const uint64_t CYCLES = I_SIZE[3] / 64 * I_SIZE[3];
const uint64_t FIRST16GB = CYCLES - ((int64_t)1 << 34)/64; // don't ask

const char *INFILES[4] = {"/mnt/nvme0/mid.bin", "/mnt/nvme0/mid.bin", "/mnt/nvme0/large.bin", "/mnt/nvme0/huge.bin"};
char *OUTFILES[4] = {(char*)"/mnt/nvme0/mid.out.0", (char*)"/mnt/nvme0/mid.out.0", (char*)"/mnt/nvme0/large.out.0", (char*)"/mnt/nvme0/huge.out.0"};

struct test_config {
    uint64_t img_addr;
    uint64_t img_write_addr;
    uint64_t krnl_lower8;
    uint64_t krnl_shift_upper;
} test_config;

#include <iostream>
#include <fstream>
void getState(aos_client *ao) {
using namespace std;
	ofstream myfile5("final_state.txt");
	uint64_t cycles = 12345;
	for (uint64_t addres = 0x20; addres <= 0x238; addres += 8) {
		usleep(100);
		ao->aos_cntrlreg_read(addres, cycles);
		//printf("addres %lx: %ld\n", addres, cycles);
		myfile5 << "Address " << hex << addres << ": " << dec << cycles << "\n";
		if (addres == 0x198) addres = 0x200 - 8;
	}
	myfile5.close();
}

int main(int argc, char *argv[]) {
	int version = 0;
	int selector = -1;
	uint64_t num_apps = 1;
	uint64_t mode = 0;// 0 = default, 1 = coyote tlb (cutoff ranges 0 to 2^20 controlling probability of tlb fault out of 2^20), 2 = physical access
	const uint64_t coyote_cutoff = 0;
	if (argc > 1) version = atol(argv[1]);
	if (argc > 2) num_apps = atol(argv[2]);
	if (argc > 3) mode = atol(argv[3]);
	if (num_apps > 4 || num_apps < 1) num_apps = 1;
	if (argc > 4) selector = atol(argv[4]);
	
	//test_config.size_shift = 6;
	//if (argc > 3) test_config.size_shift = atol(argv[3]);
	//if (test_config.size_shift > 6) test_config.size_shift = 6;
	test_config.krnl_lower8 = 0x0201020402010201;
	test_config.krnl_shift_upper = 0x0401;
	
	high_resolution_clock::time_point start, end, half;
	duration<double> diff;
	double seconds;
	
	printf("Starting new AOS client\n");
	aos_client *aos[4];
	for (uint64_t app = 0; app < num_apps; ++app) {
		aos[app] = new aos_client();
		aos[app]->set_slot_id(0);
		aos[app]->set_app_id(app);
		aos[app]->connect();
		aos[app]->aos_set_mode(mode, coyote_cutoff);
	}
	
	int fd_in[4];
	int fd_out[4];
	const char *fnames_in[4] = {INFILES[version], INFILES[version], INFILES[version], INFILES[version]};
	//char *fnames_out[4] = {OUTFILES[version], OUTFILES[version], OUTFILES[version], OUTFILES[version]};
/*
	for (uint32_t i = 1; i < num_apps; i++) {
		uint64_t len = sizeof(fnames_out[i]);
		fnames_out[i][len - 1] = '0' + i;
		std::cout << "set filename " << i << " to " << fnames_out[i] << std::endl;
	}
*/
	uint64_t fnlen = std::strlen(OUTFILES[version]);
	printf("Opening files\n");
	for (uint64_t app = 0; app < num_apps; ++app) {
		aos[app]->aos_file_open(fnames_in[app], fd_in[app]);
		char *fname = new char[fnlen+1];
		fname[fnlen] = 0;
		memcpy(fname, OUTFILES[version], fnlen);
		fname[fnlen - 1] = '0' + app;
		aos[app]->aos_file_open(fname, fd_out[app]);
		//aos[app]->aos_file_open("/mnt/tmpfs/file0.bin", fd[app]);
		printf("App %lu opened file %s at %d and %d\n", app, fname, fd_in[app], fd_out[app]);
		//delete fname;
	}
	
	void *addr_read[4];
	void *addr_write[4];
	uint64_t length = I_SIZE2[version];
	for (uint64_t app = 0; app < num_apps; ++app) {
		addr_read[app] = nullptr;
		addr_write[app] = nullptr;
		
		start = high_resolution_clock::now();
		aos[app]->aos_mmap(addr_read[app], length, PROT_READ, 0, fd_in[app], 0);
		aos[app]->aos_mmap(addr_write[app], length, PROT_WRITE, 0, fd_out[app], 0);
		end = high_resolution_clock::now();
		diff = end - start;
		seconds = diff.count();
		printf("App %lu mmaped file %d at %p and %d at %p in %gs\n", app, fd_in[app], addr_read[app], fd_out[app], addr_write[app], seconds);
	}
	
	for (int i = 0; i <= (selector == -1 ? 1 : selector); ++i) {
		// start runs
		//start = high_resolution_clock::now();
			//uint64_t test;
		start = high_resolution_clock::now();
		for (uint64_t app = 0; app < num_apps; ++app) {
		//uint64_t app = 0;
			test_config.img_addr = (uint64_t)addr_read[app];
			test_config.img_write_addr = (uint64_t)addr_write[app];

			aos[app]->aos_cntrlreg_write(0x00, test_config.img_addr);
			aos[app]->aos_cntrlreg_write(0x08, test_config.img_write_addr);
			aos[app]->aos_cntrlreg_write(0x10, test_config.krnl_lower8);
			aos[app]->aos_cntrlreg_write(0x18, test_config.krnl_shift_upper);
			printf("run %ld started\n", app);
		}
		
		// end runs
		bool hasSeen16 = version != 3; // we don't care when version != 3
		uint64_t disp = 0;
		for (uint64_t app = 0; app < num_apps; ++app) {
			uint64_t cycles = 123456789;
			do {
			//} while (true);
				aos[app]->aos_cntrlreg_read(0x108, cycles);
				if (disp++ % 10000 == 0) {
					printf("cycles left: %ld\n", cycles);
					getState(aos[app]);
				}
				if (!hasSeen16 && cycles < FIRST16GB) {
					hasSeen16 = true;
					half = high_resolution_clock::now();
				}
				if (cycles == 0) break;
				else usleep(SLEEP_TIME[version]);
			//printf("cycles left: %ld\n", cycles);
			} while (true);
			printf("run %ld done\n", app);
		}
		getState(aos[0]);
		end = high_resolution_clock::now();
		
		//uint64_t max_cycles = 1234;
		//aos[0]->aos_cntrlreg_read(0x238, max_cycles);

		// print stats
		uint64_t total_bytes = 2 * num_apps * I_SIZE2[version];
		diff = end - start;
		seconds = diff.count();
		double throughput = ((double)total_bytes)/seconds/(1<<20);
		printf("%lu conv e2e version %d: %lu bytes read in %g seconds for %g MiB/s\n", num_apps, version, total_bytes, seconds, throughput);
		if (version == 3 && hasSeen16) {
			duration<double> diff12 = half - start;
			double seconds12 = diff12.count();
			double throughput12 = 2*num_apps*((double)((int64_t)1 << 34))/seconds12/(1<<20);
			printf("First 16 GB: %g seconds, %g MiB/s\n", seconds12, throughput12);
			uint64_t leftover = (total_bytes) - 2*num_apps*((int64_t)1 << 34);
			duration<double> diff22 = end - half;
			double seconds22 = diff22.count();
			double throughput22 = ((double)leftover)/seconds22/(1<<20);
			printf("Last %lu bytes: %g seconds, %g MiB/s\n", leftover, seconds22, throughput22);
			if (selector == i or selector == -1) {
				printf("%g %g %g\n", seconds, seconds12, seconds22);
				printf("%g %g %g\n", throughput, throughput12, throughput22);
			}
		}
		else {
			if (selector == i or selector == -1) {
				printf("%g\n", seconds);
				printf("%g\n", throughput);
			}
		}
	}
	
	for (uint64_t app = 0; app < num_apps; ++app) {
		aos[app]->aos_munmap(addr_read[app], length);
		aos[app]->aos_munmap(addr_write[app], length);
		aos[app]->aos_file_close(fd_in[app]);
		aos[app]->aos_file_close(fd_out[app]);
	}
	
	return 0;
}

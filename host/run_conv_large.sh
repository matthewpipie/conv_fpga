# 
set -e
goodAGFI="agfi-01a25f7cd768efdfb"
res=`sudo fpga-describe-local-image -S0 | grep $goodAGFI | wc -l`
cmd="./conv 2"
if [ "$res" -ne 1 ]; then
	echo "Error! wrong agfi loaded. running"
	echo "sudo fpga-load-local-image -S 0 -I $goodAGFI"
	sudo fpga-load-local-image -S 0 -I $goodAGFI
	#exit 1
fi
sudo killall "daemon"
sleep 1
(cd && sudo bash addxdma.sh)
sleep 1
sudo "../daemon/daemon"
sleep 1
if [ "$3" -eq 0 ]; then
	sync
	sudo sysctl -w vm.drop_caches=3
	$cmd $1 $2 0
	exit 0
fi
#cp big.bin /dev/null
$cmd $1 $2 `echo "$3-1"|bc`
